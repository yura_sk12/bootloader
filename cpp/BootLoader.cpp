/*
 * BoolLoader.cpp
 *
 *  Created on: 24 ����. 2019 �.
 *      Author: yura
 */

#include <BootLoader.h>

namespace boot {

BootLoader::BootLoader(uint32_t jump_addr) {
	this->jump_addr = jump_addr;
	this->userAppFunc = (UserAppFunc)(*( uint32_t*) (this->jump_addr + 4));
//	__disable_irq();
}

BootLoader::~BootLoader() {
}

void BootLoader::set_stack_pointer_addr(){
	__set_MSP(*(volatile uint32_t*) this->jump_addr);
}

void BootLoader::jump_to_user_app(){
	this->set_stack_pointer_addr();
	this->userAppFunc();
}

} /* namespace boot */
