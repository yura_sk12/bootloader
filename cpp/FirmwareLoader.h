/*
 * FirmwareLoader.h
 *
 *  Created on: 24 ����. 2019 �.
 *      Author: yura
 */

#ifndef FIRMWARELOADER_H_
#define FIRMWARELOADER_H_

#include "Flashmem.h"
#include "main.h"
#include "custom_queue.h"
#include "BootLoader.h"
#ifdef STM32F303xC

#endif  // STM32F303xC

#define UART_LOADER

class FirmwareLoader {
public:
	// q - ������� � ������� ������������ ������ �� ���� ������
	FirmwareLoader(c_queue *q);
	virtual ~FirmwareLoader();
	static uint8_t calc_check_sum(uint8_t *data, uint16_t len);

	enum TT_TYPE {
		BIN_DATA = 0,
		END_DATA = 1,
		SEGMENT_ADDR = 2,
		BIG_ADDR = 4,
	};

// methods
public:
#ifdef UART_LOADER
	// ������ �� ������ HEX ����� � �������� ����� while
	void parse_and_write_to_flash_data();
#endif

// vars
private:
#ifdef UART_LOADER
	c_queue *q = nullptr;
	Flashmem *flash_mem = nullptr;
	uint32_t addr = 0;
#endif

// Methods
private:
	uint8_t __pop(c_queue *q);
};

#endif /* FIRMWARELOADER_H_ */
