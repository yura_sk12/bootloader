/*
 * BoolLoader.h
 *
 *  Created on: 24 ����. 2019 �.
 *      Author: yura
 */

#include "Flashmem.h"
#include "stdint.h"

#ifdef STM32F303xC
#include "stm32f3xx.h"
#include "stm32f303xc.h"
#endif

#ifndef BOOLLOADER_H_
#define BOOLLOADER_H_

namespace boot {
typedef void(*UserAppFunc)(void);

class BootLoader {
public:
	BootLoader(uint32_t jump_addr);
	virtual ~BootLoader();

// vars
private:
	UserAppFunc userAppFunc;
	uint32_t jump_addr;

// Methods
public:
//	void set_vec_table_addr();
	void set_stack_pointer_addr();
	void jump_to_user_app();

// Methods
private:

};

} /* namespace boot */

#endif /* BOOLLOADER_H_ */
