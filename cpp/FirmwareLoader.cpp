/*
 * FirmwareLoader.cpp
 *
 *  Created on: 24 ����. 2019 �.
 *      Author: yura
 */

#include <FirmwareLoader.h>

FirmwareLoader::FirmwareLoader(c_queue *q) {
	this->q = q;
	this->flash_mem = new Flashmem(USER_APP_PAGE_NUM);
}

FirmwareLoader::~FirmwareLoader() {
#ifdef UART_LOADER

#endif
}


#ifdef UART_LOADER  //****************UART LOADER*******************************

void FirmwareLoader::parse_and_write_to_flash_data(){
	if(!queue_is_empty(this->q)){
		// ��������� ������� �������� ������
		auto usart_transmit = [](const char *arr, uint16_t size){
			for (int var = 0; var < size; ++var) {
				while(!LL_USART_IsActiveFlag_TC(USART1)){}
				LL_USART_TransmitData8(USART1, arr[var]);
			}
		};

		if (__pop(q) == ':'){
			while(queue_size(this->q) < 4){}  // ���� ��������� LL AAAA TT
			uint8_t LL = __pop(q);								// ����� ���� ������
			uint16_t AAAA = (__pop(q) << 8);					// ������ �� TT, �� � ����������� ������� ��� �����
			AAAA |= __pop(q);
			uint8_t TT = __pop(q);								// �������

			while(queue_size(this->q) < (char)(LL + 1)){}  		// ���� TT ���� ������ + checksum
			uint8_t all_recv_data[LL + 4];						// ������ ��� ���������� ������
			uint8_t data[LL];									// ������ ������ ������� ����� �������� � ������
			all_recv_data[0] = LL;
			all_recv_data[1] = (uint8_t)((AAAA >> 8) & 0xFF);
			all_recv_data[2] = (uint8_t)(AAAA & 0xFF);
			all_recv_data[3] = TT;
			for (int i = 4; i < 4+LL; ++i) {
				all_recv_data[i] = __pop(q);
				data[i - 4] = all_recv_data[i];
			}
			uint8_t check_sum = __pop(q);
			uint8_t recive_check_sum = this->calc_check_sum(all_recv_data, LL + 4);


			// �������� ����������� �����
			if (check_sum == recive_check_sum){

				// ���������� ���������
				switch (TT) {
					case BIN_DATA: {
						this->addr &= ~(0xFFFF);
						this->addr |= AAAA;
						flash_mem->flash_mem_write_by_addr(data, addr, LL);
						break;
					}
					case END_DATA: {
						usart_transmit("COMPLETE\r\n", 10);
						usart_transmit("ACK\r\n", 5);
						LL_mDelay(10);
						boot::BootLoader loader = boot::BootLoader(0x8004000);
						loader.jump_to_user_app();
						break;
					}
					case SEGMENT_ADDR: {
						// Todo
						break;
					}
					case BIG_ADDR: {
						this->addr &= ~(0xFFFF << 16);
						this->addr = ((all_recv_data[4] << 8) | all_recv_data[5]) << 16;
						break;
					}
					default:
						break;
				}

				usart_transmit("ACK\r\n", 5);
			} else {
				usart_transmit("NACK\r\n", 6);
			}
		}
	}
}

#endif				//****************UART LOADER*******************************

uint8_t FirmwareLoader::calc_check_sum(uint8_t *data, uint16_t len){
	uint32_t sum = 0;
	for (int i = 0; i < len; ++i) {
		sum += data[i];
	}
	return 0x01 + ~(sum);
}

uint8_t FirmwareLoader::__pop(c_queue *q){
	return queue_pop(this->q);
}
