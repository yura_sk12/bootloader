/*
 * led.cpp
 *
 *  Created on: 13 ���. 2019 �.
 *      Author: yura
 */

#include <Led.h>
#include <exception>

Led::Led(GPIO_TypeDef *GPIO, uint32_t pin, bool state) {
	this->gpio = GPIO;
	this->pin = pin;
	this->state = state;
	this->init();
	this->__set_state();
}

Led::~Led() {

}

void Led::init(){
	this->init_rcc();
#ifdef STM32F303xC
	this->gpio->MODER |= (1 << (this->pin << 1));
	this->gpio->MODER &= ~(0x10 << (this->pin << 1));
	this->gpio->OTYPER &= ~(1 << this->pin);
#endif
}

void Led::set_state(bool on){
	this->state = on;
	this->__set_state();
}

void Led::toggle(){
	this->state = !this->state;
	this->__set_state();
}

void Led::__set_state(){
	if (this->state)
		this->gpio->BSRR |= 1 << this->pin;
	else
		this->gpio->BRR |= 1 << this->pin;
}

void Led::init_rcc(){
	if (this->gpio == GPIOA){
		RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	}
	if (this->gpio == GPIOB){
		RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
	}
	if (this->gpio == GPIOC){
		RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	}
	if (this->gpio == GPIOD){
		RCC->AHBENR |= RCC_AHBENR_GPIODEN;
	}
	if (this->gpio == GPIOE){
		RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
	}
	if (this->gpio == GPIOF){
		RCC->AHBENR |= RCC_AHBENR_GPIOFEN;
	}
}
