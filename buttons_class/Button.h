/*
 * Button.h
 *
 *  Created on: 25 ����. 2019 �.
 *      Author: yura
 */

#ifdef STM32F303xC
#include "stm32f303xc.h"
#ifdef __cplusplus
 extern "C" {
 	 void EXTI0_IRQHandler();
 }
#endif  // __cplusplus
#endif  // STM32F303xC

#ifndef BUTTON_H_
#define BUTTON_H_

class Button {

public:
	enum FRONT_TRIGGER{
		RISING_TRIGGER,
		FALLING_TRIGGER,
		BOTH_TRIGGER,
	};

	Button(GPIO_TypeDef *gpio, uint32_t pin, FRONT_TRIGGER trigger);
	virtual ~Button();

// methods
public:
	void init_rcc();
	void init_IT();
	void init();

// vars
private:
	GPIO_TypeDef *gpio = nullptr;
	uint32_t pin = 0;
	FRONT_TRIGGER trigger = RISING_TRIGGER;
};

#endif /* BUTTON_H_ */
