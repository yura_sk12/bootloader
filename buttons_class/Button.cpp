/*
 * Button.cpp
 *
 *  Created on: 25 ����. 2019 �.
 *      Author: yura
 */

#include <Button.h>

Button::Button(GPIO_TypeDef *gpio, uint32_t pin, FRONT_TRIGGER trigger) {
	this->gpio = gpio;
	this->pin = pin;
	this->trigger = trigger;
	this->init();
}

Button::~Button() {

}


void Button::init_rcc(){
#ifdef STM32F303xC
	if (this->gpio == GPIOA){
		RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	}
	if (this->gpio == GPIOB){
		RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
	}
	if (this->gpio == GPIOC){
		RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	}
	if (this->gpio == GPIOD){
		RCC->AHBENR |= RCC_AHBENR_GPIODEN;
	}
	if (this->gpio == GPIOE){
		RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
	}
	if (this->gpio == GPIOF){
		RCC->AHBENR |= RCC_AHBENR_GPIOFEN;
	}
#endif
}

void Button::init_IT(){
#ifdef STM32F303xC
	// ��������� ����������
	EXTI->IMR |= 1 << this->pin;

	// ��������� ���������� ���������
	if (this->pin == 0){
		NVIC_EnableIRQ(EXTI0_IRQn);
	}
	if (this->pin == 1){
		NVIC_EnableIRQ(EXTI1_IRQn);
	}
	if (this->pin == 2){
		NVIC_EnableIRQ(EXTI2_TSC_IRQn);
	}
	if (this->pin == 3){
		NVIC_EnableIRQ(EXTI3_IRQn);
	}
	if (this->pin == 4){
		NVIC_EnableIRQ(EXTI4_IRQn);
	}
	if ((this->pin >= 5) && (this->pin <=9)){
		NVIC_EnableIRQ(EXTI9_5_IRQn);
	}
	if ((this->pin >= 10) && (this->pin <=15)){
		NVIC_EnableIRQ(EXTI15_10_IRQn);
	}
#endif
}

void Button::init(){
	this->init_rcc();
#ifdef STM32F303xC
	// ���������� �� input ��� stm32f3
	this->gpio->MODER &= ~(0b11 << (this->pin << 1));
	this->gpio->PUPDR |= (0b10 << (this->pin << 1));
	this->gpio->PUPDR &= ~(0b01 << (this->pin << 1));

	// �������������� � GPIO �� EXTI
	uint8_t conf_bits = 0;
	if (this->gpio == GPIOA){
		conf_bits = 0b0000;
	}
	if (this->gpio == GPIOB){
		conf_bits = 0b0001;
	}
	if (this->gpio == GPIOC){
		conf_bits = 0b0010;
	}
	if (this->gpio == GPIOD){
		conf_bits = 0b0011;
	}
	if (this->gpio == GPIOE){
		conf_bits = 0b0100;
	}
	if (this->gpio == GPIOF){
		conf_bits = 0b0101;
	}
	SYSCFG->EXTICR[this->pin % 4] &= ~(0b0000);
	SYSCFG->EXTICR[this->pin % 4] |= conf_bits;

	// ��������� �������
	switch (this->trigger) {
		case RISING_TRIGGER: {
			EXTI->RTSR |= 1 << this->pin;
			break;
		}
		case FALLING_TRIGGER: {
			EXTI->FTSR |= 1 << this->pin;
			break;
		}
		case BOTH_TRIGGER: {
			EXTI->RTSR |= 1 << this->pin;
			EXTI->FTSR |= 1 << this->pin;
			break;
		}
		default:
			break;
	}
#endif
	this->init_IT();
}

void EXTI0_IRQHandler(){
	if (EXTI->PR & EXTI_PR_PR0)
	{
		EXTI->IMR |= EXTI_IMR_IM0;
	}
}












